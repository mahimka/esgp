import React, { Fragment } from 'react'
import html from 'react-inner-html';
import Tippy from '@tippyjs/react/headless';
import { Waypoint } from 'react-waypoint';
import { gsap } from "gsap/dist/gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { EasePack } from 'gsap/all'

import Dotlines from '../dotlines'

export default class S0 extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      animation: false,
      direction: `top`,
      direction2: `top`,
      popper: {}

    }

    this.setTippyDirection = this.setTippyDirection.bind(this);
    this.setTippyPopper = this.setTippyPopper.bind(this);
    this.setTippyDirection2 = this.setTippyDirection2.bind(this);
  }

  componentDidMount() {
    this.sectors = document.querySelectorAll(`.esgp-sector-sector > svg`);
    gsap.set(this.sectors, {scale:0.25, opacity:0});

    this.meal = document.querySelectorAll(`.esgp-meal`);
    this.megafon = document.querySelectorAll(`.esgp-megafon`);
    this.zoom1 = document.querySelector(`.esgp-zoom1`);
    gsap.set(this.zoom1, {transformOrigin:"50.0% 50.0%"})

    gsap.set(`.esgp-sign`, {opacity: 0});
    gsap.set(`.esgp-pen`, {x: -15, y: 5});

    this.sign = gsap.timeline({repeat: -1, repeatDelay: 0, paused: true });
    this.sign.set(`.esgp-sign`, {opacity: 0});
    this.sign.set(`.esgp-pen`, {x: -15, y: 5});

    this.sign.to(`.esgp-sign`, {opacity: 1, duration: 1}, 0);
    this.sign.to(`.esgp-pen`, {duration: 0.3, x: -10, y: 7}, 0);
    this.sign.to(`.esgp-pen`, {duration: 0.3, x: -5, y: 5}, 0.3);
    this.sign.to(`.esgp-pen`, {duration: 0.4, x: 0, y: 0}, 0.6);
    this.sign.to(`.esgp-pen`, {duration: 1, x: 0, y: 0}, 1);




  }

  componentDidUpdate(prevProps, prevState, snapshot){
      if(prevState.animation != this.state.animation){
        if(this.state.animation){
          this.startAnimations()
        }else{
          this.stopAnimations()
        }
      }
  }

  startAnimations(){
      gsap.killTweensOf(this.meal);
      gsap.set(this.meal, {rotation:0})
      gsap.set(`.esgp-meal[data-id="0"]`, {transformOrigin:"60.0% 50.0%"})
      gsap.set(`.esgp-meal[data-id="1"]`, {transformOrigin:"51.0% 60.0%"})
      gsap.to(this.meal, 5, {rotation:360, repeat: -1, ease: "none"});

      gsap.killTweensOf(this.megafon);
      gsap.set(this.megafon, {opacity:0})
      gsap.to(this.megafon[0], {opacity:1, ease: "linear", duration: 0.5, yoyo: true, repeat: -1, delay: 0});
      gsap.to(this.megafon[1], {opacity:1, ease: "linear", duration: 0.5, yoyo: true, repeat: -1, delay: 0.25});
      gsap.to(this.megafon[2], {opacity:1, ease: "linear", duration: 0.5, yoyo: true, repeat: -1, delay: 0.5});

      gsap.killTweensOf(this.zoom1);
      gsap.to(this.zoom1, {opacity:1, x: 20, y: -40, scale: 1.2, ease: "linear", duration: 2.5, yoyo: true, repeat: -1, repeatDelay: 1});

      this.sign.seek(0);
      this.sign.play();
  }

  stopAnimations(){
      gsap.killTweensOf(this.meal);
      gsap.set(this.meal, {rotation:0})

      gsap.killTweensOf(this.megafon);
      gsap.set(this.megafon, {opacity:0})

      gsap.killTweensOf(this.zoom1);
      gsap.set(this.zoom1, {opacity:1, x: 0, y: 0, scale: 1, ease: "linear"});
      
      this.sign.pause();
  }

  handleWaypointEnter(props){
    this.setState({animation: true});
  }
  handleWaypointLeave(props){
    this.setState({animation: false});
  }

  handleWaypointEnter1(props){
    gsap.killTweensOf([this.sectors]);
    gsap.set(this.sectors, {scale:0.25, opacity:0});
    gsap.to(this.sectors, {scale: 1.01, opacity:1, ease: "power2.out", duration: 0.5, stagger: 0.1});
  }
  handleWaypointLeave1(props){
    gsap.killTweensOf([this.sectors]);
    gsap.set(this.sectors, {scale:0.25, opacity:0});
  }

  onMount(){
    gsap.killTweensOf(`.esgp-index-tooltip`);
    gsap.set(`.esgp-index-tooltip`, {opacity: 0, y: 10, transformOrigin:"50% 50%"});
    gsap.to(`.esgp-index-tooltip`, {opacity: 1, y:0, duration: 0.5, ease: `power2.out`});
  }
  onHide(){
    gsap.set(`.esgp-index-tooltip`, {opacity: 0});
  }

  onMount1(){
    gsap.killTweensOf(`.esgp-sector-tooltip`);
    gsap.set(`.esgp-sector-tooltip`, {opacity: 0, y: 10, transformOrigin:"50% 50%"});
    gsap.to(`.esgp-sector-tooltip`, {opacity: 1, y:0, duration: 0.5, ease: `power2.out`});
  }
  onHide1(){
    gsap.set(`.esgp-sector-tooltip`, {opacity: 0});
  }

  onMount2(){
    gsap.killTweensOf(`.esgp-sheme-tooltip`);
    gsap.set(`.esgp-sheme-tooltip`, {opacity: 0, y: 10, transformOrigin:"50% 50%"});
    gsap.to(`.esgp-sheme-tooltip`, {opacity: 1, y:0, duration: 0.5, ease: `power2.out`});
  }
  onHide2(){
    gsap.set(`.esgp-sheme-tooltip`, {opacity: 0});
  }


  setTippyDirection(dir, popper){
    this.setState({direction: dir, popper: popper})
  }
  setTippyPopper(popper){
    this.setState({popper: popper})
  }
  setTippyDirection2(dir){
    this.setState({direction2: dir})
  }

  render() {
    const data = this.props.data;

    let direction = this.state.direction
    let direction2 = this.state.direction2
    let that = this

    const topLogger = {
      name: 'topLogger',
      enabled: true,
      phase: 'main',
      fn({ state }) {
        if( direction!= state.placement){
          that.setTippyDirection(state.placement)
        }
      },
    };

    const topLogger2 = {
      name: 'topLogger',
      enabled: true,
      phase: 'main',
      fn({ state }) {
        if( direction2!= state.placement){
          that.setTippyDirection2(state.placement)
        }
      },
    };

    return (
        <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
          onEnter={(props) => {this.handleWaypointEnter(props)}}
          onLeave={(props) => {this.handleWaypointLeave(props)}}
        >
          <section className={`esgp-section`} data-id={"1"} >
            
            <div className={`esgp-section-absolute`}>
              <div className={`esgp-list-svg`} {...html(require(`!raw-loader!../../assets/list.svg`))} />
              <Dotlines dtid={3} color={`#525456`}/>
              <Dotlines dtid={4} color={`#525456`}/>
            </div>

            <div className={`esgp-row`} data-id={"1-0"}>
              <h2 className={`esgp-h2 open-sans-bold`} data-id={"1"} dangerouslySetInnerHTML={{__html: data.h2 }} />
              <p className={`esgp-p`} data-id={"1-0"}>{data.p[0]}</p>
              <p className={`esgp-p`} data-id={"1-1"}>{data.p[1]}</p>
              <p className={`esgp-p`} data-id={"1-2"}>{data.p[2]}</p>
            </div>

            <div className={`esgp-row`} data-id={"1-1"}>
              <p className={`esgp-h3 open-sans-bold text-center`} data-id={"1"}>{
                (window.mobileCheck)?data.diagram.h3m:data.diagram.h3
                }</p>

              <Waypoint scrollableAncestor={window} bottomOffset={"30%"} fireOnRapidScroll={false}
                onEnter={(props) => {this.handleWaypointEnter1(props)}}
                onLeave={(props) => {this.handleWaypointLeave1(props)}}
              >
                <div className={`esgp-diagram-container`}>
                  <div className={"esgp-diagram"} {...html(require(`!raw-loader!../../assets/diagram.svg`))} />
                  
                  <div className={`esgp-diagram-sectors`}>
                  {
                    data.diagram.sectors.map((d,i) => (
                      <div 
                        key={i}
                        data-id={i}
                        className={`esgp-seсtor-sector-container`}
                      >
                        <Tippy
                          onMount={this.onMount1}
                          onHide={this.onHide1}
                          popperOptions={{
                            modifiers: [topLogger]
                          }}
                          render={attrs => (
                            <div className={`esgp_tooltip esgp-sector-tooltip` + ((this.state.direction==`top`)?` `:` esgp-sector-tooltip-bottom`)} data-id={"1-3-" + i} tabIndex="-1" {...attrs}>
                              <p className={`esgp-tooltip1-name open-sans-semibold`}>{d.indicators.name}</p>
                              <div className={`esgp-tooltip-line`} />
                              {
                                d.indicators.indicators.map((q,j) => (
                                  <div className={`esgp-tooltip-span-container`} key={j}>
                                    <span className={`esgp-tooltip-span0`}>{q.name + ` `}</span>
                                    <span className={`esgp-tooltip-span1`}>{`(`+q.source+`)`}</span>
                                  </div>
                                ))
                              }
                            </div>
                          )}
                        >
                          <div className={"esgp-sector-sector"} data-id={i} {...html(require(`!raw-loader!../../assets/sector${i}.svg`))} />
                        </Tippy>
                      </div>
                    ))
                  }
                  </div>
                  {
                    data.diagram.sectors.map((d,i) => (
                      <div 
                        key={i}
                        data-id={i}
                        className={`esgp-seсtor-button-container`}
                      >
                        <Tippy
                          onMount={this.onMount2}
                          onHide={this.onHide2}
                          placement={`top`}
                          popperOptions={{
                            modifiers: [topLogger2]
                          }}
                          render={attrs => (
                            <div className={`esgp_tooltip esgp-sheme-tooltip` + ((this.state.direction2==`top`)?` `:` esgp-sheme-tooltip-bottom`)} data-id={"1-2-" + i} tabIndex="-1" {...attrs}>
                              <p className={`egsp-s1-p`} dangerouslySetInnerHTML={{__html: d.p }} />
                            </div>
                          )}
                        >
                          <div className={"esgp-sector-button open-sans-bold"} data-id={i} >{ d.name }</div>
                        </Tippy>
                      </div>
                    ))
                  }
                </div>
              </Waypoint>
            </div>

            <div className={`esgp-row esgp-row1-2`} data-id={"1-2"}>
              <p className={`esgp-h3 open-sans-bold`} data-id={"1-1"}>{
                (window.mobileCheck)?data.indexes.h3m:data.indexes.h3
              }</p>
             
               <div className={`esgp-indexes`}>
                {
                  data.indexes.indexes.map((d,i) => (
                    <div 
                      key={i}
                      data-id={i}
                      className={``}
                    >
                      <Tippy
                        onMount={this.onMount}
                        onHide={this.onHide}
                        render={attrs => (
                          <div className="esgp_tooltip esgp-index-tooltip" data-id={"1"} tabIndex="-1" {...attrs}>
                            <p className="esgp-index-tooltip-p">{d.value}</p>
                          </div>
                        )}
                      >
                        <div className={"esgp-index"} >{ d.name }</div>
                      </Tippy>
                    </div>
                  ))
                }
              </div>
            </div>

          </section>
        </Waypoint>

    );
  }
}
