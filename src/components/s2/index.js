import React from 'react';
import html from 'react-inner-html';

import { Waypoint } from 'react-waypoint';
import { gsap } from "gsap/dist/gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { EasePack } from 'gsap/all'

// import { VectorMap } from 'react-jvectormap';

import Tippy from '@tippyjs/react/headless';

import Dotlines from '../dotlines'
import Roundbtn from '../roundbtn'

import _sortBy from 'lodash/sortBy';

export default class S2 extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      country: -1,
      default: { id: -1, name: 'Select country'},
      animation: false
    }
    this.countryChange = this.countryChange.bind(this);
    
    this.startAnimations = this.startAnimations.bind(this);
    this.stopAnimations = this.stopAnimations.bind(this);
    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
    this.handleWaypointLeave = this.handleWaypointLeave.bind(this);
    this.changeCountry = this.changeCountry.bind(this);
    this.changeCountry2 = this.changeCountry2.bind(this);

  }

  componentDidMount() {

    let ships = document.querySelectorAll(`.esgp-ship`);
    gsap.set(ships, {transformOrigin:"50% 50%"});

    this.ship0 = gsap.timeline({repeat: -1, repeatDelay: 0, paused: true });
    this.ship0.set(ships[0], {transformOrigin:"50% 50%"})
    this.ship0.set(ships[0], {scaleX:-1});
    this.ship0.to(ships[0], {duration: 10, x: 80, ease: `linear`});
    this.ship0.set(ships[0], {scaleX:1});
    this.ship0.to(ships[0], {duration: 20, x: -80, ease: `linear`});
    this.ship0.set(ships[0], {scaleX:-1});
    this.ship0.to(ships[0], {duration: 10, x: 0, ease: `linear`});

    this.ship1 = gsap.timeline({repeat: -1, repeatDelay: 0, paused: true });
    this.ship1.set(ships[1], {transformOrigin:"50% 50%"})
    this.ship1.set(ships[1], {scaleX:1});
    this.ship1.to(ships[1], {duration: 10, x: -40, ease: `linear`});
    this.ship1.set(ships[1], {scaleX:-1});
    this.ship1.to(ships[1], {duration: 20, x: 40, ease: `linear`});
    this.ship1.set(ships[1], {scaleX:1});
    this.ship1.to(ships[1], {duration: 10, x: 0, ease: `linear`});

    this.ship2 = gsap.timeline({repeat: -1, repeatDelay: 0, paused: true });
    this.ship2.set(ships[2], {transformOrigin:"50% 50%"})
    this.ship2.set(ships[2], {scaleX:-1});
    this.ship2.to(ships[2], {duration: 10, x: -50, ease: `linear`});
    this.ship2.set(ships[2], {scaleX:1});
    this.ship2.to(ships[2], {duration: 20, x: 50, ease: `linear`});
    this.ship2.set(ships[2], {scaleX:-1});
    this.ship2.to(ships[2], {duration: 10, x: 0, ease: `linear`});

    this.upwaves = document.querySelectorAll(`.esgp-up-wave`);
    this.downwaves = document.querySelectorAll(`.esgp-down-wave`);
    this.mappoints = document.querySelectorAll(`.esgp-map-point`);
    gsap.set(this.mappoints, {transformOrigin:"50% 50%", scale:0, opacity: 0});
    this.gears = document.querySelectorAll(`.esgp-gear`);



    if(!window.mobileCheck){
      gsap.set(`.esgp-gears-svg`, {y: 150, transformOrigin:"50% 50%"})
      const tl5 = gsap.timeline({
        scrollTrigger: {
          trigger: ".esgp-row1-2",
          start: "top bottom",
          end: "bottom top",
          scrub: 0.25,
          markers: false
        }
      });
      tl5.set(`.esgp-gears-svg`, {y: 150})
      tl5.to(`.esgp-gears-svg`, {y: -30, duration: 1}, 0)
    }


  }

  componentDidUpdate(prevProps, prevState, snapshot){
      if(prevState.animation != this.state.animation){
        if(this.state.animation){
          this.startAnimations()
        }else{
          this.stopAnimations()
        }
      }
  }

  startAnimations(){
    this.ship0.seek(0);
    this.ship0.play();

    this.ship1.seek(0);
    this.ship1.play();

    this.ship2.seek(0);
    this.ship2.play();

    gsap.killTweensOf([this.upwaves, this.downwaves]);
    gsap.to(this.upwaves, {x: 10, repeat:-1, yoyo: true, ease:`linear`, duration: 4});
    gsap.to(this.downwaves, {x: -10, repeat:-1, yoyo: true, ease:`linear`, duration: 4});
  }
  stopAnimations(){
    this.ship0.pause();
    this.ship1.pause();
    this.ship2.pause();

    gsap.killTweensOf([this.upwaves, this.downwaves]);
    gsap.set(this.upwaves, {x: 0});
    gsap.set(this.downwaves, {x: 0});
  }

  handleWaypointEnter(props){ 
    this.setState({animation: true});
  }
  handleWaypointLeave(props){
    this.setState({animation: false});
  }

  handleWaypointEnter1(props){
    gsap.killTweensOf([this.mappoints]);
    gsap.set(this.mappoints, {transformOrigin:"50% 50%", scale:0, opacity: 0});
    gsap.to(this.mappoints, {scale: 1, opacity:1, ease: "back.out(2)", delay: 0.25, duration: 0.3, stagger: 0.04});
  }
  handleWaypointLeave1(props){
    gsap.killTweensOf([this.mappoints]);
    gsap.to(this.mappoints, {scale: 0, opacity:0, ease: "power1.out", delay: 0, duration: 0.2, stagger: 0});
  }

  handleWaypointEnter2(props){
      gsap.killTweensOf([this.gears]);
      gsap.set(this.gears, {transformOrigin:"50% 50%", rotation:0})
      gsap.to(this.gears[0], 5, {rotation:360, repeat: -1, ease: "none"});
      gsap.to(this.gears[1], 15, {rotation:-360, repeat: -1, ease: "none"});
      gsap.to(this.gears[2], 5, {rotation:360, repeat: -1, ease: "none"});
  }
  handleWaypointLeave2(props){
      gsap.killTweensOf([this.gears]);
      gsap.set(this.gears, {rotation:0})
  }

  countryChange(event){
    this.setState({country: event.target.value});
  }

  onMount(){
    gsap.killTweensOf(`.esgp-map-tooltip`);
    gsap.set(`.esgp-map-tooltip`, {opacity: 0, y: 5, transformOrigin:"50% 100%", scale:0.5});
    gsap.to(`.esgp-map-tooltip`, {opacity: 1, scale:1, duration: 0.5, ease: `power2.out`});
  }
  onHide(){
    gsap.set(`.esgp-map-tooltip`, {opacity: 0});
  }

  changeCountry(id){
    this.setState({country: id})
  }

  changeCountry2(e) {
    this.setState({country: e.target.value})
  }

  render() {
    const data = this.props.data;
    const map = _sortBy(data.map, 'country');

    return (
        <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
          onEnter={(props) => {this.handleWaypointEnter(props)}}
          onLeave={(props) => {this.handleWaypointLeave(props)}}
        >
          <section className={`esgp-section`} data-id="2" >
            <img src={`img/line1to2.svg`} className={`esgp-line1to2`} />

            {/* <div className={`esgp-section-bg`} {...html(require(`!raw-loader!../../assets/bg2.svg`))} /> */}


            <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
              onEnter={(props) => {this.handleWaypointEnter2(props)}}
              onLeave={(props) => {this.handleWaypointLeave2(props)}}
            >
              <div className={`esgp-gears-svg`} {...html(require(`!raw-loader!../../assets/gears.svg`))} />
            </Waypoint>


            <div className={`esgp-gull-svg`} {...html(require(`!raw-loader!../../assets/gull.svg`))} />
            <Dotlines dtid={5} color={`#fff`} alpha={0.8}/>
            <Dotlines dtid={6} color={`#fff`} alpha={0.8}/>

            <div className={`esgp-row`} data-id={"2-0"}>
              <h2 className={`esgp-h2 open-sans-bold`} data-id={"2"} dangerouslySetInnerHTML={{__html: data.h2 }} />
              <p className={`esgp-p`} data-id={"2-0"}>{data.p}</p>
              <p className={`esgp-h3 open-sans-bold`} data-id={"2"}>{
                (window.mobileCheck)?data.h3m:data.h3
              }</p>

                <div className={`select-container`}>
                <div className={`select-btn`} {...html(require(`!raw-loader!../../assets/selectbtn.svg`))} />

                <select 
                  className="esgp-select"
                  value={this.state.country}
                  onChange={this.changeCountry2}
                >
                <option value={this.state.default.id} >{this.state.default.name}</option>
                {
                  map.map((d,i) => (
                    <option
                      key={i}
                      value={d.id}
                    >{d.country}</option>
                    ))
                }
                </select>
                </div>

                <div className={`esgp-map-container`}>

{/*
                  <VectorMap map={'world_mill'}
                     backgroundColor="transparent"
                     zoomOnScroll={false}
                     ref="map"
                     containerStyle={{
                         width: '100%',
                         height: '100%'
                     }}
                     containerClassName="map"
                  />
*/}

                  <img className={`esgp-map`} src={`img/map.png`} srcSet={`img/map_2x.png 2x`} alt={`map`} />



                  <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
                    onEnter={(props) => {this.handleWaypointEnter1(props)}}
                    onLeave={(props) => {this.handleWaypointLeave1(props)}}
                  >

                    <div className={`esgp-overmap`}  >

                        <div className={`esgp-overmap-svg`} {...html(require(`!raw-loader!../../assets/overmap.svg`))} />

                        <img className={`esgp-overmap-svg1`} data-id={``} src={`img/overmap.svg`} alt={``} />



                        {
                          data.map.map((d,i) => (
                            <Tippy
                              key={i}
                              onMount={this.onMount}
                              onHide={this.onHide}
                              appendTo={`parent`}
                              visible={ (this.state.country == d.id)? true : false}
                              onClickOutside={(e) => this.setState({country:-1}) }
                              render={attrs => (
                                <div 
                                  className={`esgp_tooltip esgp-map-tooltip` + ((d.direction > 0)?` esgp-map-tooltip-positive`:((d.direction < 0)?` esgp-map-tooltip-negative`:``)) } 
                                  data-id={i} 
                                  tabIndex="-1" 
                                  {...attrs}
                                >
                                  <p className={`esgp-map-tooltip-txt open-sans-bold`}>{d.country + `: ` + ((d.direction > 0)?`Positive`:((d.direction < 0)?`Negative`:`Neutral`))}</p>
                                  {/*<p className={`esgp-map-tooltip-txt open-sans-bold`}>{d.country + `: ` + d.score + ` (` + ((d.direction > 0)?`Positive`:((d.direction < 0)?`Negative`:`Neutral`)) + `)`}</p>*/}
                                </div>
                              )}
                            >
                              <div
                                className={`esgp-map-point` +  ( (this.state.country == d.id) ?    ((d.direction > 0)?` esgp-map-point-positive`:(   (d.direction < 0)?` esgp-map-point-negative`:` esgp-map-point-zero`)   )   :``) }
                                data-id={i}
                                onClick={(e) => this.changeCountry(d.id)}
                              />
                            </Tippy>
                          ))
                        }
                    </div>
                  </Waypoint>

                </div>
                <p className={`esgp-map-source`} >{data.source}</p>
            </div>

            <Roundbtn data={data.extra} dtid={0}/>

            <img src={`img/line2to3.svg`} className={`esgp-line2to3`} />

          </section>
        </Waypoint>

    );
  }
}
