import React, { useRef } from 'react'
import html from 'react-inner-html';

import { Waypoint } from 'react-waypoint';
import { gsap, EasePack } from 'gsap/all'

import Dotlines from '../dotlines'

export default class Quotes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {animation: false}
    this.myRef = React.createRef();

    this.startAnimations = this.startAnimations.bind(this);
    this.stopAnimations = this.stopAnimations.bind(this);
    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
    this.handleWaypointLeave = this.handleWaypointLeave.bind(this);
  }

  componentDidMount() {
      this.gears0 = document.querySelectorAll(`.esgp-gear0`);
      this.gears1 = document.querySelectorAll(`.esgp-gear1`);
  }

  componentDidUpdate(prevProps, prevState, snapshot){
      if(prevState.animation != this.state.animation){
        if(this.state.animation){
          this.startAnimations()
        }else{
          this.stopAnimations()
        }
      }
  }


  startAnimations(){
      gsap.killTweensOf([this.gears0, this.gears1]);
      gsap.set([this.gears0, this.gears1], {transformOrigin:"50% 50%", rotation:0})
      gsap.to(this.gears0[0], 5, {rotation:360, repeat: -1, ease: "none"});
      gsap.to(this.gears0[1], 15, {rotation:-360, repeat: -1, ease: "none"});
      gsap.to(this.gears0[2], 5, {rotation:360, repeat: -1, ease: "none"});

      gsap.to(this.gears1[0], 5, {rotation:360, repeat: -1, ease: "none"});
      gsap.to(this.gears1[1], 15, {rotation:-360, repeat: -1, ease: "none"});
      gsap.to(this.gears1[2], 5, {rotation:360, repeat: -1, ease: "none"});

  }

  stopAnimations(){
      gsap.killTweensOf([this.gears0, this.gears1]);
      gsap.set(this.gears0, {rotation:0})
      gsap.set(this.gears1, {rotation:0})

  }


  handleWaypointEnter(props){ 
    this.setState({animation: true});
  }
  handleWaypointLeave(props){
    this.setState({animation: false});
  }

  render() {

    return (
      <Waypoint
        scrollableAncestor={window}
        bottomOffset={"0%"}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
        fireOnRapidScroll={false}
      >
        <div className={`esgp-quotes-acontainer esgp-quotes-${this.props.qid}` + ((this.props.active)?` esgp-quotes-active`:``)}>
        <div className={`esgp-quotes-container`}>
        <div className={`esgp-quotes-container-h100`}>

          {/*
          <blockquote className={`esgp-quotes`} ref={this.myRef} data-id={this.props.qid}>
            <div className={`esgp-start-quote`} {...html(require(`!raw-loader!../../assets/start-quote.svg`))}/>
            <div className={`esgp-finish-quote`} {...html(require(`!raw-loader!../../assets/finish-quote.svg`))}/>
            <div className={`esgp-quote open-sans-light`} dangerouslySetInnerHTML={{__html: this.props.quote }} />
            <div className={`esgp-autor`} dangerouslySetInnerHTML={{__html: this.props.autor }} />
          </blockquote>
          */}

          <Dotlines dtid={7} color={`#707070`}/>
          <Dotlines dtid={8} color={`#707070`}/>

          {
            (this.props.qid==0) ?
            <div className={`esgp-quotes-gear1`} {...html(require(`!raw-loader!../../assets/quotes-gear1.svg`))} />
            :
            <div className={`esgp-quotes-gear2`} {...html(require(`!raw-loader!../../assets/quotes-gear2.svg`))} />
          }
          {
            (this.props.qid==0) ?
            <div className={`esgp-quotes-gear0`} {...html(require(`!raw-loader!../../assets/quotes-gear0.svg`))} />
            :
            <div className={`esgp-quotes-gear3`} {...html(require(`!raw-loader!../../assets/quotes-gear3.svg`))} />
          }

        </div>
        </div>
        </div>

      </Waypoint>
    );
  }
}