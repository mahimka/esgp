import React, { useRef } from 'react'
import html from 'react-inner-html';

import { Waypoint } from 'react-waypoint';
import { gsap, EasePack } from 'gsap/all'
import Quote from '../../assets/quote1.svg'
// import QuoteM from '../../assets/quote1m.svg'
import './quotespace.css'

import Dotlines from '../dotlines'

export default class QuoteSpace1 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  componentDidMount() {
    this.handleWaypointLeave();
  }

  componentDidUpdate(prevProps, prevState, snapshot){

  }

  handleWaypointEnter(props){
  }

  handleWaypointLeave(props){
  }

  handleWaypointEnter2(props){
      gsap.killTweensOf("#esgp-zoom");
      gsap.set("#esgp-zoom", {scale: 1, transformOrigin: "50% 50%"});
      gsap.to("#esgp-zoom", {scale: 1.6, ease: "power1.out", delay: 0.0, duration: 2.3, repeat: -1, repeatDelay: 1, yoyo: true });
  }
  handleWaypointLeave2(props){
      gsap.killTweensOf("#esgp-zoom");
     gsap.to("#esgp-zoom", {scale: 1, ease: "power1.out", delay: 0.0, duration: 1.3, repeat: 0, yoyo: false });
  }


  render() {
    const data = this.props.data;
    return (
      <Waypoint
        scrollableAncestor={window}
        bottomOffset={"0%"}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
        fireOnRapidScroll={false}
      >
        <div className={`esgp-quotes-box`} data-id={`0`}>
          
          <div className={`esgp-quote-space-1`}>
            <Quote />
          </div>

          <div className={`esgp-row`} data-id={"001"}>

            <p className={`esgp-p text-center`} data-id={"0-0"}>{data.p[0]}</p>
            <p className={`esgp-p text-center`} data-id={"0-1"}>{data.p[1]}</p>
            <p className={`esgp-p text-center`} data-id={"0-2"}>{data.p[2]}</p>
            <p className={`esgp-h3 open-sans-semibold text-center`} data-id={"0"}>{data.p[3]}</p>

          </div>


                <Dotlines dtid={1} color={`#525456`}/>
                <Dotlines dtid={2} color={`#525456`}/>


                <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
                  onEnter={(props) => {this.handleWaypointEnter2(props)}}
                  onLeave={(props) => {this.handleWaypointLeave2(props)}}
                >
                  <div className={`esgp-globe`} {...html(require(`!raw-loader!../../assets/globe.svg`))}/>
                </Waypoint>


          <blockquote className={`esgp-quotes`} data-id={this.props.qid}>
            <div className={`esgp-start-quote`} {...html(require(`!raw-loader!../../assets/start-quote.svg`))}/>
            <div className={`esgp-finish-quote`} {...html(require(`!raw-loader!../../assets/finish-quote.svg`))}/>
            <div className={`esgp-quote open-sans-light`} dangerouslySetInnerHTML={{__html: this.props.quote }} />
            <div className={`esgp-autor`} dangerouslySetInnerHTML={{__html: this.props.autor }} />
          </blockquote>



        </div>
      </Waypoint>
    );
  }
}