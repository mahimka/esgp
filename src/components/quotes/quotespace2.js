import React, { useRef } from 'react'
import html from 'react-inner-html';

import { Waypoint } from 'react-waypoint';
import { gsap, EasePack } from 'gsap/all'
import Quote from '../../assets/quote2.svg'
import './quotespace.css'

export default class QuoteSpace2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  componentDidMount() {
    this.handleWaypointLeave();
  }

  componentDidUpdate(prevProps, prevState, snapshot){

  }

  handleWaypointEnter(props){
    // let root = document.querySelector('.esgp')
    // root.classList.add('esgp_2')
    // console.log('2 Enter')
  }

  handleWaypointLeave(props){
    // let root = document.querySelector('.esgp')
    // root.classList.remove('esgp_2')
    // console.log('2 Leave')
  }

  render() {

    return (
      <Waypoint
        scrollableAncestor={window}
        bottomOffset={"0%"}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
        fireOnRapidScroll={false}
      >
        <div className={`esgp-quote-space-2`}>
          {/* <Quote /> */}

          <blockquote className={`esgp-quotes`} data-id={this.props.qid}>
            <div className={`esgp-start-quote`} {...html(require(`!raw-loader!../../assets/start-quote.svg`))}/>
            <div className={`esgp-finish-quote`} {...html(require(`!raw-loader!../../assets/finish-quote.svg`))}/>
            <div className={`esgp-quote open-sans-light`} dangerouslySetInnerHTML={{__html: this.props.quote }} />
            <div className={`esgp-autor`} dangerouslySetInnerHTML={{__html: this.props.autor }} />
          </blockquote>

        </div>
      </Waypoint>
    );
  }
}