import React from 'react';
import html from 'react-inner-html';
import { Waypoint } from 'react-waypoint';
import { gsap } from "gsap/dist/gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import {EasePack, ScrollToPlugin} from 'gsap/all'
import Graph from '../../assets/s3_graph.svg'
import SvgNotebook from '../../assets/s3_notebook2.svg'
// import GraphLeft from '../../../static/data/s3/s3_graph_left.svg'
import './index.css'

import Dotlines from '../dotlines'
import Roundbtn from '../roundbtn'


let data = {
  root: null,
  text: null,
  rhombus: null,
  translate: '',
  translateArr: [],
}

export default class S3 extends React.Component {
  constructor(props) {
    super(props);
    gsap.registerPlugin(ScrollToPlugin);
    this.state={
      svgRoot: null,
      selected: false,
      default: { id: 'default', name: 'Select country'},
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleWaypointEnter1 = this.handleWaypointEnter1.bind(this);
    this.handleWaypointLeave1 = this.handleWaypointLeave1.bind(this);
  }
  
  componentDidUpdate(prevProps, prevState, snapshot){
  }

  getElements (name) {
    const root = document.querySelector(`[data-id='${name}']`)
    this.svgRoot.appendChild(root)
    const text = root.querySelector('text')
    const rhombus = root.querySelector('rect')
    return { root, text, rhombus }
  }

  showPoint(val) {
    let countryItem = this.props.data.graph.filter(f=>f.slug === val)[0]
    // console.log(countryItem)
    data = this.getElements(val)
    // console.log(data)
    data.root.classList.add('g_active')
    data.translateArr = [data.root.getAttribute('data-trx'), data.root.getAttribute('data-try')]
    let x = parseFloat(data.translateArr[0]);
    let y = parseFloat(data.translateArr[1]);
    data.root.style.transform = `scale(1.5)`
    data.root.setAttribute('transform', `translate(${x} ${y})`)
    data.root.style.transformOrigin = `${x + countryItem.x}px ${y + countryItem.y}px`

    let elem = document.querySelector(`.sip_county_graph_svg g[data-id="${val}"]`)
    gsap.to(`.sip_county_graph_svg_w`, {duration: 1, scrollTo: {x:elem, offsetX: 100}, ease: "power2.inOut"});
  }

  clearPoints(val) {
    if(!data.root) return
    data.root.classList.remove('g_active')
    data.root.style.transform = `scale(1)`
  }

  translateParse(str) {
    str = str.replace('translate(', '')
    str = str.replace(')', '')
    return str.split(' ')
  }

  handleChange(event) {
    if(event.target.value !== 'default') {
      this.clearPoints(event.target.value)
      this.showPoint(event.target.value)
    }else{
      this.clearPoints(event.target.value)
    }
  }

  makeID (str) {
    return str.replace(/ /ig, '_').toLowerCase()
  }

  prepateSVG () {
    this.svgRoot = document.querySelector("[data-name='country_list']");
    let gList = this.svgRoot.querySelectorAll("[data-name='cntry']");
    gList.forEach(m=>{
      let name = this.makeID(m.querySelector('text').textContent)
      let tr = this.translateParse(m.querySelector('text').getAttribute('transform'))
      m.setAttribute('data-trx', tr[0])
      m.setAttribute('data-try', tr[1])
      m.setAttribute('data-id', name)
    })
  }

  componentDidMount() {
    this.prepateSVG()
    gsap.set("[data-name='cntry']", { opacity: 0 })

    this.gears = document.querySelectorAll(`.esgp-gear3`);


    if(!window.mobileCheck){
      gsap.set(`.esgp-s3-notebooks-div`, {y: 65, transformOrigin:"50% 50%"})
      const tl5 = gsap.timeline({
        scrollTrigger: {
          trigger: ".esgp-s3-notebooks-div",
          start: "top bottom",
          end: "bottom top",
          scrub: 0.25,
          markers: false
        }
      });
      tl5.set(`.esgp-s3-notebooks-div`, {y: 65})
      tl5.to(`.esgp-s3-notebooks-div`, {y: -65, duration: 1}, 0)
    }




    let elem = document.querySelector(`.sip_county_graph_svg g[data-id="india"]`)
    gsap.to(`.sip_county_graph_svg_w`, {duration: 0, scrollTo: {x:elem, offsetX: 100}});
  }

  handleWaypointEnter(props){
    gsap.killTweensOf("[data-name='cntry']");
    gsap.to("[data-name='cntry']", {
      opacity: 1,
      stagger: 0.02
    });
  }
  handleWaypointLeave(props){
    gsap.set("[data-name='cntry']", { opacity: 0 })
  }


  handleWaypointEnter1(props){
    gsap.killTweensOf([this.gears]);
    gsap.set(this.gears, {transformOrigin:"50% 50%", rotation:0})
    gsap.to(this.gears[0], 5, {rotation:360, repeat: -1, ease: "none"});
    gsap.to(this.gears[1], 15, {rotation:-360, repeat: -1, ease: "none"});
    gsap.to(this.gears[2], 5, {rotation:360, repeat: -1, ease: "none"});
  }
  handleWaypointLeave1(props){
    gsap.killTweensOf([this.gears]);
    gsap.set(this.gears, {rotation:0})
  }



  render() {
    const data = this.props.data;
    return (
      <section className={`esgp-section`} data-id="3" >

        <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
          onEnter={(props) => {this.handleWaypointEnter1(props)}}
          onLeave={(props) => {this.handleWaypointLeave1(props)}}
        >
          <div className={`esgp-s3-notebooks-div`}>
            <SvgNotebook className={`esgp-s3-notebooks`} />
            <div className={`esgp-cloud3 esgp-note-4`} {...html(require(`!raw-loader!../../assets/s3-cloud.svg`))} />
          </div>
        </Waypoint>




        <img src={`data/s3/s3_bg.svg`} className={`esgp-section-img`}/>



        <div className={`esgp-row`} data-id={"1"}>
          
          <div className={`esgp-row1-bg`} />

          <Dotlines dtid={9} color={`#fff`} alpha={0.8}/>
          <h2 className="esgp-h2 open-sans-bold">{data.h2}</h2>
          {
            data.p.map((p,i) => {
              return <p className="esgp-p esgp-p3" key={i} data-id={`${i}`}>{p}</p>
            })
          }
          <h3 className="esgp-h3 open-sans-bold" data-id={"3"}>{data.h3}</h3>



          <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
            onEnter={(props) => {this.handleWaypointEnter(props)}}
            onLeave={(props) => {this.handleWaypointLeave(props)}}
          >
            <div className="sip_county_graph">
              

                <div className={`select-container`}>
                
                <div className={`select-btn`} {...html(require(`!raw-loader!../../assets/selectbtn.svg`))} />



              <select
                className={`esgp-select`}
                value={this.state.value}
                onChange={this.handleChange}
              >
                <option value={this.state.default.id} >{this.state.default.name}</option>
                {
                  data.graph.map(country => {
                    return <option value={country.slug} key={country.id}>{country.name}</option>
                  })
                }
              </select>


              </div>

              <div className="sip_county_graph_svg_w">
                <div className="sip_county_graph_svg">
                  <Graph />
                </div>
                <div className={`esgp-screp0`} {...html(require(`!raw-loader!../../assets/s3_screp0.svg`))} />
                <div className={`esgp-screp1`} {...html(require(`!raw-loader!../../assets/s3_screp0.svg`))} />
              </div>

              <div className="sip_county_graph_svg_mob">
                <img src={`data/s3/s3_graph_left.svg`} />
              </div>

            </div>
          </Waypoint>
            
          <p className={`esgp-row-space-between`}>
            <small className="esgp-small">{data.source[0]}</small>
            <small className="esgp-small">{data.source[1]}</small>
          </p>


          <Roundbtn data={data.extra} dtid={1}/>

        </div>
      </section>
    );
  }
}
