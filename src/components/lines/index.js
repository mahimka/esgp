import React, { Fragment } from 'react'
import html from 'react-inner-html';

export default class Lines extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  componentDidMount() {
  }

  handleResize() {

  }

  componentDidUpdate(prevProps, prevState, snapshot){

  }

  render() {
    const color = this.props.color;


    return (
        <Fragment>
          <div 
            lines={`esgp-lines`} 
            {...html(require(`!raw-loader!../../assets/lines.svg`))} 
          />
        </Fragment>
    );
  }
}
