import React from 'react';
import html from 'react-inner-html';

import { Waypoint } from 'react-waypoint';
import { gsap } from "gsap/dist/gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { EasePack } from 'gsap/all'
import Graph from '../../assets/s4_graph.svg'
import './index.css'
// import SvgNotebook from '../../assets/s3-s4_notebooks.svg'
import SvgNotebook0 from '../../assets/s3-s4_notebooks0.svg'
import SvgNotebook1 from '../../assets/s3-s4_notebooks1.svg'
import SvgNotebook2 from '../../assets/s3-s4_notebooks2.svg'
import SvgNotebook3 from '../../assets/s3-s4_notebooks3.svg'

import S4Hand from '../../assets/s4_hand.svg'
import S4Handhand from '../../assets/right-hand.svg'
import S4Gears from '../../assets/s4_gears.svg'
import Tippy from '@tippyjs/react/headless';
// import 'tippy.js/dist/tippy.css';
import Dotlines from '../dotlines'
import Roundbtn from '../roundbtn'

export default class S4 extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      modal: false,
    }

    this.handleWaypointEnter3 = this.handleWaypointEnter3.bind(this);
    this.handleWaypointLeave3 = this.handleWaypointLeave3.bind(this);

    this.handleWaypointEnter4 = this.handleWaypointEnter4.bind(this);
    this.handleWaypointLeave4 = this.handleWaypointLeave4.bind(this);

  }

  componentDidMount() {
    gsap.set(".sip_graph_half_4 .sip_ig", {height: '0%'});
    gsap.set(".sip_graph_half_4 .sip_ig_btn", {opacity: 0, scale: 0.8});


    if(!window.mobileCheck){

    gsap.set(`.esgp-s4-notebooks0`, {y: 80, transformOrigin:"50% 50%"})
    gsap.set(`.esgp-s4-notebooks1`, {y: 30, transformOrigin:"50% 50%"})
    gsap.set(`.esgp-s4-notebooks2`, {y: 50, transformOrigin:"50% 50%"})
    gsap.set(`.esgp-s4-notebooks3`, {y: 70, transformOrigin:"50% 50%"})

    const tl0 = gsap.timeline({
      scrollTrigger: {
        trigger: ".esgp-s4-notebooks0",
        start: "top bottom",
        end: "bottom top",
        scrub: 0.25,
        markers: false
      }
    });
    tl0.set(`.esgp-s4-notebooks0`, {y: 80})
    tl0.set(`.esgp-s4-notebooks1`, {y: 30})
    tl0.set(`.esgp-s4-notebooks2`, {y: 50})
    tl0.set(`.esgp-s4-notebooks3`, {y: 70})

    tl0.to(`.esgp-s4-notebooks0`, {y: -80, duration: 1}, 0)
    tl0.to(`.esgp-s4-notebooks1`, {y: -30, duration: 1}, 0)
    tl0.to(`.esgp-s4-notebooks2`, {y: -50, duration: 1}, 0)
    tl0.to(`.esgp-s4-notebooks3`, {y: -70, duration: 1}, 0)


    gsap.set(`.esgp-s4-notebooks3`, {y: 0, transformOrigin:"50% 50%"})
    const tl1 = gsap.timeline({
      scrollTrigger: {
        trigger: ".esgp-s4-hand",
        start: "top bottom",
        end: "bottom top",
        scrub: 0.25,
        markers: false
      }
    });
    tl1.set(`.esgp-s4-hand`, {y: 0})
    tl1.to(`.esgp-s4-hand`, {y: -150, duration: 1}, 0)

    }

    this.gears4 = document.querySelectorAll(`.esgp-gear4`);
    this.gears3 = document.querySelectorAll(`.esgp-gear5`);

    gsap.set(`.esgp-table[data-id="4"]`, {opacity:0, transformOrigin:"50% 50%", scale: 0.85})

  }

  handleWaypointEnter(props){
    
    gsap.killTweensOf([
      ".sip_ig",
      ".sip_ig_btn"
    ]);

    gsap.to(".sip_graph_half_4 .sip_ig_1", {height: '45.2%', duration: 0.5});
    gsap.to(".sip_graph_half_4 .sip_ig_2", {height: '32.8%', duration: 0.5, delay: 0.2});
    gsap.to(".sip_graph_half_4 .sip_ig_3", {height: '42.01%', duration: 0.5, delay: 0.4});
    gsap.to(".sip_graph_half_4 .sip_ig_4", {height: '53.3%', duration: 0.5, delay: 0.6});
    gsap.to(".sip_graph_half_4 .sip_ig_5", {height: '78.7%', duration: 0.5, delay: 0.8});
    gsap.to(".sip_graph_half_4 .sip_ig_6", {height: '80.6%', duration: 0.5, delay: 1.0});
    gsap.to(".sip_graph_half_4 .sip_ig_7", {height: '80.8%', duration: 0.5, delay: 1.2});
    gsap.to(".sip_graph_half_4 .sip_ig_btn_2", {opacity: 1, scale: 1, duration: 0.3, delay: 1.0});
    gsap.to(".sip_graph_half_4 .sip_ig_btn_1", {opacity: 1, scale: 1, duration: 0.3, delay: 1.2});
  }
  handleWaypointLeave(props){

    gsap.killTweensOf([
      ".sip_ig",
      ".sip_ig_btn"
    ]);

    gsap.to(".sip_graph_half_4 .sip_ig", {height: '0%', duration: 0.2});
    gsap.to(".sip_graph_half_4 .sip_ig_btn", {opacity: 0, duration: 0.2, scale: 0.8});
  }


  handleWaypointEnter3(props){
      gsap.killTweensOf([this.gears3]);
      gsap.set(this.gears3, {transformOrigin:"50% 50%", rotation:0})
      gsap.to(this.gears3[0], 10, {rotation:360, repeat: -1, ease: "none"});
      gsap.to(this.gears3[1], 15, {rotation:-360, repeat: -1, ease: "none"});
  }
  handleWaypointLeave3(props){
      gsap.killTweensOf([this.gears3]);
      gsap.set(this.gears3, {rotation:0})
  }


  handleWaypointEnter4(props){
      gsap.killTweensOf([this.gears4]);
      gsap.set(this.gears4, {transformOrigin:"50% 50%", rotation:0})
      gsap.to(this.gears4[0], 5, {rotation:360, repeat: -1, ease: "none"});
      gsap.to(this.gears4[1], 15, {rotation:-360, repeat: -1, ease: "none"});
      gsap.to(this.gears4[2], 5, {rotation:360, repeat: -1, ease: "none"});
  }
  handleWaypointLeave4(props){
      gsap.killTweensOf([this.gears4]);
      gsap.set(this.gears4, {rotation:0})
  }

  handleWaypointEnter2(props){
      gsap.killTweensOf([`.esgp-table[data-id="4"]`]);
      gsap.set(`.esgp-table[data-id="4"]`, {opacity:0, scale: 0.85})
      gsap.to(`.esgp-table[data-id="4"]`, {opacity:1, scale: 1, ease: "power4.out", duration: 1});

  }
  handleWaypointLeave2(props){
      gsap.killTweensOf([`.esgp-table[data-id="4"]`]);
      gsap.set(`.esgp-table[data-id="4"]`, {opacity:0, scale: 0.85})
  }


  openModal(e, val) {
    if(!e) {
      this.setState( {modal: val });
    }
    if(e && e.target.classList.contains('esgp-modal-w')) {
      this.setState( {modal: false });
    }

    if(val) {
      document.body.classList.add('body_modal')
    }else{
      document.body.classList.remove('body_modal')
    }


  }

  render() {
    const data = this.props.data;

    const Modal = () => {
      return (
        <div
          data-id='modal'
          role='dialog'
          onClick={(e) => this.openModal(e, false)}
          className={`esgp-modal-w` + (this.state.modal ? ' esgp-modal-w-show' : '')}
        >
          <div
            className={`esgp-modal`}
            style={{backgroundImage: `url(img/bg_modal.svg)`}}
            >
            <h1 className={`esgp-modal-h1 open-sans-bold`}>{data.extra.name}</h1>
            
            <div className={`esgp-modal-img-block`}>
              <img className={`esgp-modal-img`} src="img/modal.svg" />
              <div className={`esgp-modal-img-txt`} {...html(require(`!raw-loader!../../assets/modal-txt.svg`))} />
            </div>


            <div className={`esgp-modal-row`}>

            <div className={`esgp-modal-left`}>
              <h3 className={`esgp-modal-h3 open-sans-bold`}>{data.extra.left.h3}</h3>

              <ul>
                {
                  data.extra.left.ul.map((li,i) => {
                    return <li className="esgp-p esgp-p-modal" key={i} dangerouslySetInnerHTML={{__html: li}} />
                  })
                }
              </ul>
            </div>

            <div className={`esgp-modal-right`}>
              <h3 className={`esgp-modal-h3 open-sans-bold`}>{data.extra.right.h3}</h3>

              <ul>
                {
                  data.extra.right.ul.map((li,i) => {
                    return <li className="esgp-p esgp-p-modal" key={i} dangerouslySetInnerHTML={{__html: li}} />
                  })
                }
              </ul>
            </div>

            </div>


            <button
              className={`esgp-btn-close`}
              aria-label={`Close`}
              onClick={(e) => this.openModal(null, false)}
            />

          </div>
        </div>
      )
    };

    return (
      <section className={`esgp-section`} data-id="4" >
        
        <Dotlines dtid={10} color={`#fff`} alpha={0.8}/>
        <SvgNotebook0 className={`esgp-s4-notebooks esgp-s4-notebooks0`} />
        <SvgNotebook1 className={`esgp-s4-notebooks esgp-s4-notebooks1`} />
        <SvgNotebook2 className={`esgp-s4-notebooks esgp-s4-notebooks2`} />
        <SvgNotebook3 className={`esgp-s4-notebooks esgp-s4-notebooks3`} />

        <div className={`esgp-row`} data-id={"1"}>

          <div
            className={`esgp-btn-modal`}
            onClick={(e) => this.openModal(null, true)}
            >
              <Roundbtn data={data.extra} dtid={2}/>
          </div>
          


          <h2 className="esgp-h2 open-sans-bold">{data.h2}</h2>

          <p className="esgp-p">{data.p}</p>

          <h3 className="esgp-h3 open-sans-bold">{
            (window.mobileCheck)?data.h3m:data.h3
          }</h3>


          <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
            onEnter={(props) => {this.handleWaypointEnter(props)}}
            onLeave={(props) => {this.handleWaypointLeave(props)}}
          >

            <div className={`sip_graph_half_w`}>

              <div className={`sip_graph_half sip_graph_half_4`}>
                <Graph />
                <div className="sip_ig sip_ig_1" />
                <div className="sip_ig sip_ig_2" />
                <div className="sip_ig sip_ig_3" />
                <div className="sip_ig sip_ig_4" />
                <div className="sip_ig sip_ig_5" />
                <div className="sip_ig sip_ig_6" />
                <div className="sip_ig sip_ig_7" />

                <Tippy
                  render={attrs => (
                    <div className="esgp_tooltip" tabIndex="-1" {...attrs}>
                      <h1>{data.graph[1].name}</h1>
                      <p>{data.graph[1].p}</p>
                    </div>
                  )}
                >
                  <div className="sip_ig_btn sip_ig_btn_1" />
                </Tippy>

                <Tippy
                  render={attrs => (
                    <div className="esgp_tooltip" tabIndex="-1" {...attrs}>
                      <h1>{data.graph[0].name}</h1>
                      <p>{data.graph[0].p}</p>
                    </div>
                  )}
                >
                  <div className="sip_ig_btn sip_ig_btn_2" />
                </Tippy>
              </div>



              <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
                onEnter={(props) => {this.handleWaypointEnter4(props)}}
                onLeave={(props) => {this.handleWaypointLeave4(props)}}
              >
                <div className={`esgp-s4-half_image-div`} >
                <S4Hand className={`esgp-s4-half_image`} />
                </div>
              </Waypoint>


              {/* <img src={`data/s4/s4_hand.svg`} className={`esgp-s4-half_image`} /> */}

            </div>

          </Waypoint>

          <p className={`esgp-row-space-between`}>
            <small className="esgp-small">{data.source}</small>
          </p>

          <div className={`esgp-flex esgp-s4-2`}>
            
            <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
              onEnter={(props) => {this.handleWaypointEnter3(props)}}
              onLeave={(props) => {this.handleWaypointLeave3(props)}}
            >
            <div className={`esgp-s4-2-left`}>
              <S4Gears />
            </div>
            </Waypoint>



            <div className={`esgp-s4-2-right`}>
              <p>{data.poster.p}</p>
            <Waypoint scrollableAncestor={window} bottomOffset={"20%"} fireOnRapidScroll={false}
              onEnter={(props) => {this.handleWaypointEnter2(props)}}
              onLeave={(props) => {this.handleWaypointLeave2(props)}}
            >
              <div>

              {/*
              <img src={`img/s4_1.svg`} className={`esgp-table`} data-id={"4"}/>
              */}
              <div className={`esgp-table`} {...html(require(`!raw-loader!../../assets/s4_1.svg`))} data-id={"4"}/>
              

              </div>
            </Waypoint>

              <div className={`esgp-s4-2-circle`} />
            </div>
          </div>
        </div>

        <S4Handhand className={`esgp-s4-hand`} />

        <Modal />

        <img src={`data/s4/s4_bottom.svg`} className={`esgp-s4-bottom`} />

        <Dotlines dtid={11} color={`#fff`} alpha={0.8}/>


      </section>
    );
  }
}
