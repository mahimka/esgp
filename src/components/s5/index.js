import React from 'react';
import html from 'react-inner-html';

import { Waypoint } from 'react-waypoint';
import { gsap } from "gsap/dist/gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { EasePack } from 'gsap/all'
import Graph1 from '../../../static/data/s5/graph1.svg'
import Graph2 from '../../../static/data/s5/graph2.svg'
import S5Hand from '../../assets/s5_hand.svg'
// import S5Microphone from '../../assets/s5_microphone.svg'
import Tippy from '@tippyjs/react/headless';
import './index.css'
import Dotlines from '../dotlines'

let a = 0;

export default class S0 extends React.Component {
  constructor(props) {
    super(props);
    this.state={}
  }

  componentDidMount() {
    gsap.set(".sip_graph_half_5_1 .sip_ig", {height: '0%'});
    gsap.set(".sip_graph_half_5_2 .sip_ig_btn", {opacity: 0, scale: 0.8});

    gsap.set("#crane_c1", {x: '60px'})
    gsap.set("#crane_c2", {x: '-60px'})
    gsap.set("#crane_c3", {x: '60px'})
    gsap.set("#mountains", {y: '160px'})
    gsap.set("#footer-bg-image", {y: '150px'})
    gsap.set(".esgp-s5-3svg", {opacity: 0})

    const cr = gsap.timeline({
      scrollTrigger: {
        trigger: ".esgp-footer-trigger",
        start: "top bottom",
        end: "bottom top",
        scrub: 0.25,
        markers: false
      }
    });

    cr.to(`#crane_c1`, {x: '-60px', duration: 1}, 0)
    cr.to(`#crane_c2`, {x: '60px', duration: 1}, 0)
    cr.to(`#crane_c3`, {x: '-60px', duration: 1}, 0)
    cr.to(`#mountains`, {y: '20px', duration: 1}, 0)
    cr.to(`#footer-bg-image`, {y: '-60px', duration: 1}, 0)
    
    // ----------
    gsap.set(".esgp-s5-1-half_image-hand", {y: '120px'})
    const hnd = gsap.timeline({
      scrollTrigger: {
        trigger: ".esgp-s5-1-half_image-w",
        start: "top bottom",
        end: "bottom top",
        scrub: 0.25,
        markers: false
      }
    });
    hnd.to(`.esgp-s5-1-half_image-hand`, {y: '-40px', duration: 1}, 0)
    // ----------
    this.birds = gsap.timeline({repeat: -1, repeatDelay: 5, paused: true, onUpdate: function(e){
      a++;
      gsap.set('.esgp-bird', {opacity: 0});
      gsap.set(`#esgp-footer-bird${a%4}`, {opacity: 1});
    }});

    gsap.set('.esgp-bird', {opacity: 0});
    let bird = document.querySelector("#esgp-footer-bird");
    this.birds.to(bird, {duration: 0, x: 0, y: 40, opacity: 0});
    this.birds.to(bird, {duration: 0.5, x: 0, y: 40, opacity: 0});
    this.birds.to(bird, {duration: 0.3, x: 100, y: 0, opacity: 1, ease: "power1.in" });
    this.birds.to(bird, {duration: 3, x: 2000, y: -270, opacity: 1, ease: "none" });
    // ----------

    this.gears4 = document.querySelectorAll(`.esgp-gearm`);

  }

  
  handleWaypointEnter1(props){
    gsap.killTweensOf("#esgp-footer-sun");
    gsap.set("#esgp-footer-sun", {opacity:0.65})
    gsap.to("#esgp-footer-sun", {opacity:0.5, repeat: -1, ease: "power0.inOut", yoyoEase: true, duration:0.35, yoyo: true});

    gsap.killTweensOf('.sip_graph_half_5_1 .sip_ig');
    var tl = gsap.timeline();

    tl.to(".sip_graph_half_5_1 .sip_ig_1", {height: '32.8%', duration: 0.5});
    tl.to(".sip_graph_half_5_1 .sip_ig_2", {height: '31.5%', duration: 0.5}, "-=0.3");
    tl.to(".sip_graph_half_5_1 .sip_ig_3", {height: '42.9%', duration: 0.5}, "-=0.3");
    tl.to(".sip_graph_half_5_1 .sip_ig_4", {height: '47.1%', duration: 0.5}, "-=0.3");
    tl.to(".sip_graph_half_5_1 .sip_ig_5", {height: '44.1%', duration: 0.5}, "-=0.3");
    tl.to(".sip_graph_half_5_1 .sip_ig_6", {height: '55.6%', duration: 0.5}, "-=0.3");
    tl.to(".sip_graph_half_5_1 .sip_ig_7", {height: '65.8%', duration: 0.5}, "-=0.3");
    tl.to(".sip_graph_half_5_1 .sip_ig_8", {height: '63.5%', duration: 0.5}, "-=0.3");
    tl.to(".sip_graph_half_5_1 .sip_ig_9", {height: '75.5%', duration: 0.5}, "-=0.3");

  }
  handleWaypointLeave1(props){
    gsap.killTweensOf("#esgp-bg0-sun");
    gsap.killTweensOf(".sip_ig");
    gsap.set(".sip_graph_half_5_1 .sip_ig", {height: '0%'});
  }

  handleWaypointEnter2(props){
    //
    this.birds.seek(0);
    this.birds.play();
    //

    gsap.killTweensOf('.sip_graph_half_5_2 .sip_ig_btn');

    var tl = gsap.timeline();
    tl.to(".sip_graph_half_5_2 .sip_ig_btn_1", {opacity: 1, scale: 1, duration: 0.3});
    tl.to(".sip_graph_half_5_2 .sip_ig_btn_2", {opacity: 1, scale: 1, duration: 0.3}, "-=0.1");
    tl.to(".sip_graph_half_5_2 .sip_ig_btn_3", {opacity: 1, scale: 1, duration: 0.3}, "-=0.1");
    tl.to(".sip_graph_half_5_2 .sip_ig_btn_4", {opacity: 1, scale: 1, duration: 0.3}, "-=0.1");

  }
  handleWaypointLeave2(props){
    gsap.killTweensOf('.sip_graph_half_5_2 .sip_ig_btn');
    gsap.set(".sip_graph_half_5_2 .sip_ig_btn", {opacity: 0, scale: 0.8});

    if(props.currentPosition === 'below') {
      this.birds.pause();
    }
  }
  handleWaypointEnter3 () {
    gsap.killTweensOf(".esgp-s5-3svg");
    var tl = gsap.timeline();
    tl.to(".esgp-s5-3svg", {opacity: 1, duration: 1, delay: 0.5});
  }
  handleWaypointLeave3 () {
    gsap.set(".esgp-s5-3svg", {opacity: 0})
  }

  handleWaypointEnter4(props){
      gsap.killTweensOf([this.gears4]);
      gsap.set(this.gears4, {transformOrigin:"50% 50%", rotation:0})
      gsap.to(this.gears4[0], 5, {rotation:360, repeat: -1, ease: "none"});
      gsap.to(this.gears4[1], 10, {rotation:-360, repeat: -1, ease: "none"});
      gsap.to(this.gears4[2], 10, {rotation:-360, repeat: -1, ease: "none"});
  }
  handleWaypointLeave4(props){
      gsap.killTweensOf([this.gears4]);
      gsap.set(this.gears4, {rotation:0})
  }




  render() {
    const data = this.props.data;
    return (
      <section className={`esgp-section`} data-id="5" >
        <img src={`data/s5/s5_top.svg`} className={`esgp-s5-top`} />
        <div className={`esgp-row`} data-id={"5-0"}>

          <h2 className="esgp-h2 open-sans-bold">{data.h2}</h2>

          <div className="esgp-relative" >
            <Dotlines dtid={'5-1'} color={`#707070`}/>
          </div>

          {/*
            data.p.map((p,i) => {
              return <p className="esgp-p" key={i}>{p}</p>
            })
          */}

          <p className="esgp-p" key={0}>{data.p[0]}</p>


          <div className="esgp-relative" >
            <Dotlines dtid={'5-2'} color={`#707070`}/>
          </div>


          <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
            onEnter={(props) => {this.handleWaypointEnter1(props)}}
            onLeave={(props) => {this.handleWaypointLeave1(props)}}
          >

          <div className={`sip_graph_half_w_1`}>


            <div className={`sip_graph_half_5_1_w`}>

              <h3 className="esgp-h3 open-sans-bold">{
                (window.mobileCheck)?data.graph0.h3m:data.graph0.h3

              }</h3>

              <div className={`sip_graph_half_5_1`}>


                <Graph1 />
                <div className="sip_ig sip_ig_1" />
                <div className="sip_ig sip_ig_2" />
                <div className="sip_ig sip_ig_3" />
                <div className="sip_ig sip_ig_4" />
                <div className="sip_ig sip_ig_5" />
                <div className="sip_ig sip_ig_6" />
                <div className="sip_ig sip_ig_7" />
                <div className="sip_ig sip_ig_8" />
                <div className="sip_ig sip_ig_9" />

                


              </div>


              <p className={`esgp-row-space-between`}>
                  <small className="esgp-small" dangerouslySetInnerHTML={{__html: data.graph0.source }} />
              </p>

            </div>

            <div className={`esgp-s5-1-half_image-w`}>
              <S5Hand className={`esgp-s5-1-half_image`} />
              <div className={`esgp-s5-1-half_image-hand`} {...html(require(`!raw-loader!../../assets/left-hand.svg`))} />
            </div>
            {/* <img src={`img/s5_1.svg`} className={`esgp-s5-1-half_image`} /> */}
            {/* <img src={`data/s5/s5_hand.svg`} className={`esgp-s5-1-half_image`} /> */}

          </div>


          </Waypoint>




          <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
            onEnter={(props) => {this.handleWaypointEnter2(props)}}
            onLeave={(props) => {this.handleWaypointLeave2(props)}}
          >

          <div className={`sip_graph_half_w_2`}>

            <div className={`sip_graph_half_5_2_w`}>

              <h3 className="esgp-h3 open-sans-bold">{data.graph1.h3}</h3>


              <div className={`sip_graph_half_5_2`}>

                <Graph2 />
                <Tippy
                    render={attrs => (
                      <div className="esgp_tooltip" tabIndex="-1" {...attrs}>
                        <p>{data.graph1.popups[0]}</p>
                      </div>
                    )}
                  >
                    <div className="sip_ig_btn sip_ig_btn_1" />
                </Tippy>

                <Tippy
                    render={attrs => (
                      <div className="esgp_tooltip" tabIndex="-1" {...attrs}>
                        <p>{data.graph1.popups[1]}</p>
                      </div>
                    )}
                  >
                    <div className="sip_ig_btn sip_ig_btn_2" />
                </Tippy>

                <Tippy
                    render={attrs => (
                      <div className="esgp_tooltip" tabIndex="-1" {...attrs}>
                        <p>{data.graph1.popups[2]}</p>
                      </div>
                    )}
                  >
                    <div className="sip_ig_btn sip_ig_btn_3" />
                </Tippy>

                <Tippy
                    render={attrs => (
                      <div className="esgp_tooltip" tabIndex="-1" {...attrs}>
                        <p>{data.graph1.popups[3]}</p>
                      </div>
                    )}
                  >
                    <div className="sip_ig_btn sip_ig_btn_4" />
                </Tippy>



              </div>

              <p className={`esgp-row-space-between`}>
                <small className="esgp-small">{data.graph1.source}</small>
              </p>


            </div>
            <Waypoint scrollableAncestor={window} bottomOffset={"20%"} fireOnRapidScroll={false}
              onEnter={(props) => {this.handleWaypointEnter4(props)}}
              onLeave={(props) => {this.handleWaypointLeave4(props)}}
            >
            <div className={`esgp-s5-2-half_image`} {...html(require(`!raw-loader!../../assets/s5_microphone.svg`))} />
            {/* <img src={`data/s5/s5_microphone.svg`} className={`esgp-s5-2-half_image`} /> */}
            </Waypoint>
          </div>

          </Waypoint>



          <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
            onEnter={(props) => {this.handleWaypointEnter3(props)}}
            onLeave={(props) => {this.handleWaypointLeave3(props)}}
          >

              {/*
              <img src={`img/s4_1.svg`} className={`esgp-table`} data-id={"4"}/>
              */}
              
              <div className={`esgp-s5-3`}>

              <p className="esgp-p" key={2}>{data.p[2]}</p>
              <p className="esgp-p" key={1}>{data.p[1]}</p>

              <div className={`esgp-s5-3svg`} {...html(require(`!raw-loader!../../assets/s5_3.svg`))} data-id={"5"}/>

              </div>

          </Waypoint>


          {/* <img src={`img/s5_bg.png`} className={`esgp-s5-4_image`} /> */}


          
        </div>

 

          <div className={`esgp-s5-bottom-w`}>
            <div className={`esgp-s5-bottom esgp-footer-trigger`}>
              <div className={`esgp-s5-bg-svg`} {...html(require(`!raw-loader!../../assets/footer.svg`))} />

{/*
              <img className={`esgp-s5-bg-svg1`} data-id={``} src={`img/footer.svg`} alt={``} />
*/}


              <div className={`esgp-s5-bottom-slogan-w`} >
                <div className={`esgp-s5-bottom-slogan`} dangerouslySetInnerHTML={{__html: data.slogan }} />
                <div className={`esgp-s5-bottom-disclaimer`} dangerouslySetInnerHTML={{__html: data.disclaimer }} />
              </div>
            </div>
          </div>

          <div
            style={{backgroundImage: `url(img/s5_bg_m.jpg)`}}
            className={`esgp-s5-bottom-mob`}
          >
            <div className={`esgp-s5-bottom-slogan`} dangerouslySetInnerHTML={{__html: data.slogan }} />
            <div className={`esgp-s5-bottom-disclaimer`} dangerouslySetInnerHTML={{__html: data.disclaimer }} />
          </div>




        <div className={`esgp-footer`}>

        </div>
      </section>

    );
  }
}
