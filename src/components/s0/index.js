import React from 'react';
import html from 'react-inner-html';

import { Waypoint } from 'react-waypoint';
import { gsap } from "gsap/dist/gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { EasePack } from 'gsap/all'

let a = 0;

import Dotlines from '../dotlines'


export default class S0 extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      animation: false
    }

    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
    this.handleWaypointLeave = this.handleWaypointLeave.bind(this);
    this.startAnimations = this.startAnimations.bind(this);
    this.stopAnimations = this.stopAnimations.bind(this);
  }

  componentDidMount() {

    this.birds = gsap.timeline({repeat: -1, repeatDelay: 5, paused: true, onUpdate: function(e){
        a++;
        gsap.set('.esgp-bird', {opacity: 0});
        gsap.set(`#esgp-bird${a%4}`, {opacity: 1});
    }});

    gsap.set('.esgp-bird', {opacity: 0});

    let bird = document.querySelector("#esgp-bird");
    this.birds.to(bird, {duration: 0, x: 0, y: 40, opacity: 0});
    this.birds.to(bird, {duration: 0.5, x: 0, y: 40, opacity: 0});
    this.birds.to(bird, {duration: 0.3, x: 100, y: 0, opacity: 1, ease: "power1.in" });
    this.birds.to(bird, {duration: 3, x: 2000, y: -270, opacity: 1, ease: "none" });

    let plane = document.querySelector("#esgp-plane");
    gsap.set(plane, {opacity:0})
    this.planes = gsap.timeline({repeat: -1, repeatDelay: 0, paused: true });
    this.planes.to(plane, {duration: 0, x: 3000, y: 100, opacity: 1});
    this.planes.to(plane, {duration: 15, x: -3000, y: -100});

    let tree0 = document.querySelector(`.esgp-tree[data-id="0"]`);
    let tree1 = document.querySelector(`.esgp-tree[data-id="1"]`);
    let tree2 = document.querySelector(`.esgp-tree[data-id="2"]`);
    this.trees = [tree0, tree1, tree2]
    gsap.killTweensOf(this.trees);
    gsap.set(this.trees, {scale: 0, transformOrigin: "50% 100%"});

    const tl5 = gsap.timeline({
      scrollTrigger: {
        trigger: ".esgp-quote-space-1",
        start: "top bottom",
        end: "bottom top",
        scrub: 0.25,
        markers: false
      }
    });
    // tl5.set(`.esgp-quotes[data-id="0"]`, {y: 300, transformOrigin:"50% 50%"})
    tl5.set(`.esgp-quotes-gear0`, {y: 300, transformOrigin:"50% 50%"})
    tl5.set(`.esgp-quotes-gear1`, {y: 200, transformOrigin:"50% 50%"})
    // tl5.to(`.esgp-quotes[data-id="0"]`, {y: -50, duration: 1}, 0)
    tl5.to(`.esgp-quotes-gear0`, {y: -100, duration: 1}, 0)
    tl5.to(`.esgp-quotes-gear1`, {y: -200, duration: 1}, 0)

    const tl6 = gsap.timeline({
      scrollTrigger: {
        trigger: ".esgp-quote-space-2",
        start: "top bottom",
        end: "bottom top",
        scrub: 0.25,
        markers: false
      }
    });
    // tl6.set(`.esgp-quotes[data-id="1"]`, {y: 300, transformOrigin:"50% 50%"})
    tl6.set(`.esgp-quotes-gear2`, {y: 300, transformOrigin:"50% 50%"})
    tl6.set(`.esgp-quotes-gear3`, {y: 200, transformOrigin:"50% 50%"})
    // tl6.to(`.esgp-quotes[data-id="1"]`, {y: -50, duration: 1}, 0)
    tl6.to(`.esgp-quotes-gear2`, {y: -100, duration: 1}, 0)
    tl6.to(`.esgp-quotes-gear3`, {y: -200, duration: 1}, 0)


    gsap.set(`#esgp-snow0`, {y: 0, transformOrigin:"50% 50%"})
    const tl1 = gsap.timeline({
      scrollTrigger: {
        trigger: ".esgp-section0",
        start: "top bottom",
        end: "bottom top",
        scrub: 0.25,
        markers: false
      }
    });
    tl1.set(`#esgp-snow0`, {y: 150})
    tl1.to(`#esgp-snow0`, {y: 0, duration: 1}, 0)
  }

  componentDidUpdate(prevProps, prevState, snapshot){
      if(prevState.animation != this.state.animation){
        if(this.state.animation){
          this.startAnimations()
        }else{
          this.stopAnimations()
        }
      }
  }

  startAnimations(){
    gsap.killTweensOf("#esgp-mouse-arrow");
    gsap.set(`#esgp-mouse-arrow`, {y:-3})
    gsap.to("#esgp-mouse-arrow", {y: 3, repeat:-1, yoyo: true, ease:`power2.out`, duration: 0.75});

    gsap.killTweensOf("#esgp-bg0-sun");
    gsap.set("#esgp-bg0-sun", {opacity:0.65})
    gsap.to("#esgp-bg0-sun", {opacity:0.5, repeat: -1, ease: "power0.inOut", yoyoEase: true, duration:0.35, yoyo: true});

    this.birds.seek(0);
    this.birds.play();
    
    this.planes.seek(0);
    this.planes.play();
  }
  stopAnimations(){
    gsap.killTweensOf("#esgp-mouse-arrow");
    gsap.killTweensOf("#esgp-bg0-sun");

    this.birds.pause();
    this.planes.pause();
  }

  handleWaypointEnter(props){
    this.setState({animation: true});
  }
  handleWaypointLeave(props){
    this.setState({animation: false});
  }

  handleWaypointEnter1(props){
      gsap.killTweensOf(this.trees);
      gsap.set(this.trees, {scale: 0, transformOrigin: "50% 100%"});
      gsap.to(this.trees, {scale: 1, ease: "power1.out", delay: 0.255, duration: 0.3, stagger: 0.1 });
  }
  handleWaypointLeave1(props){
      gsap.killTweensOf(this.trees);
      gsap.set(this.trees, {scale: 0, transformOrigin: "50% 100%"});
  }


  render() {
    const data = this.props.data;
    return (
      <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
      >
        <section className={`esgp-section esgp-section0`} data-id="0" >

          <div className={`esgp-row`} data-id={"0"}>

            <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
              onEnter={(props) => {this.handleWaypointEnter1(props)}}
              onLeave={(props) => {this.handleWaypointLeave1(props)}}
            >
              <div className={`esgp-section-bg`} data-id={"0"}>
                <img className={`esgp-section-bg-jpg`} src={`img/bg0.jpg`} srcSet={`img/bg0_2x.jpg 2x`} data-id={"0"}/>
                <div className={`esgp-section-bg-svg`} {...html(require(`!raw-loader!../../assets/bg0.svg`))} data-id={"0"}/>
                <Dotlines dtid={0} color={`#fff`}/>

              </div>
            </Waypoint>

            <h1 className={`esgp-h1 open-sans-light text-center`}>{data.h1}</h1>

            <div id="esgp-mouse">
              <div id="esgp-mouse-img" {...html(require(`!raw-loader!../../assets/mouse.svg`))} />
              <div id="esgp-main-text" className={`open-sans-bold`} dangerouslySetInnerHTML={{__html: data.legend }} />
            </div>

{/*
            <p className={`esgp-p text-center`} data-id={"0-0"}>{data.p[0]}</p>
            <p className={`esgp-p text-center`} data-id={"0-1"}>{data.p[1]}</p>
            <p className={`esgp-p text-center`} data-id={"0-2"}>{data.p[2]}</p>
            <p className={`esgp-h3 open-sans-semibold text-center`} data-id={"0"}>{data.p[3]}</p>
*/}
          </div>

        </section>
      </Waypoint>
    );
  }
}
