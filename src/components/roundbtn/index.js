import React from 'react';
import html from 'react-inner-html';
import ReactDOM from 'react-dom';

import { Waypoint } from 'react-waypoint';
import { gsap, EasePack} from 'gsap/all'

import Tippy from '@tippyjs/react/headless';

export default class Roundbtn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hidden: true
    }

    this.myRef = React.createRef();

    this.onMount = this.onMount.bind(this);
    this.onHide = this.onHide.bind(this);
  }

  componentDidMount() {
    this.pulses = this.myRef.current.querySelectorAll(`.esgp-roundbtn-pulse`);
    gsap.set(this.pulses, {transformOrigin:"center center"})
    this.tl = gsap.timeline({repeat:-1, repeatDelay:3, paused: true});
    this.tl.fromTo(this.pulses[0], 1, {autoAlpha:0.3, scale: 1}, {autoAlpha:0, scale: 1.5, ease: "quad.inOut"})
           .fromTo(this.pulses[1], 1, {autoAlpha:0.3, scale: 1}, {autoAlpha:0, scale: 1.5, ease: "quad.inOut"}, 0.5);
  }

  handleWaypointEnter(props){
    this.tl.seek(0);
    this.tl.play();
  }

  handleWaypointLeave(props){
    this.tl.pause();
  }

  onMount(){
    if(this.props.dtid!=2){
      gsap.set(`.esgp-roundbutton-tooltip`, {opacity: 0, x: 0});
      gsap.to(`.esgp-roundbutton-tooltip`, {opacity: 1, x: -10, duration: 0.5, ease: `power2.out`});
    }
  }
  onHide(){

  }

  render() {
    const data  = this.props.data;

    return (

      <Waypoint
        scrollableAncestor={window}
        bottomOffset={"0%"}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
        fireOnRapidScroll={false}
      >

        <Tippy
          interactive={true}
          onMount={this.onMount}
          onHide={this.onHide}
          placement={(window.mobileCheck)?`top`:`left`}
          render={attrs => (
            
            (this.props.dtid!=2)
            ?
              <div 
                className={`esgp-roundbutton-tooltip`} 
                tabIndex="-1" 
                {...attrs}
              >
              {
                data.p.map((d,i) => (
                  <p key={i} className={`esgp-roundbutton-tooltip-text open-sans-regular`}>{d}</p>
                ))
              }
              </div>
            :
              <div/>
          )}
        >

          <div 
            className={"esgp-roundbtn"}
            tabIndex="0"
            role="button"
            name="did your know"
            ref={this.myRef}
            data-id={this.props.dtid} 
            onClick={(e) => { }}
          >
              <div className={`esgp-roundbtn-pulse`} data-id={`0`} />
              <div className={`esgp-roundbtn-pulse`} data-id={`1`} />
              <div className={`esgp-roundbtn-bg`} />
              <div className={`esgp-roundbtn-text open-sans-bold`}>
                <span>{data.name}</span>
              </div>
          </div>

      </Tippy>

    </Waypoint>
    );
  }
}
