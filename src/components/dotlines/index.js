import React, { useRef } from 'react'
import html from 'react-inner-html';

import { Waypoint } from 'react-waypoint';
import { gsap } from "gsap/dist/gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { EasePack } from 'gsap/all'


export default class S0 extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      animation: true
    }

    this.myRef = React.createRef();

    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
    this.handleWaypointLeave = this.handleWaypointLeave.bind(this);
    this.startAnimations = this.startAnimations.bind(this);
    this.stopAnimations = this.stopAnimations.bind(this);
  }

  componentDidMount() {
    this.paths = this.myRef.current.querySelectorAll(`path`);
    this.g = this.myRef.current.querySelectorAll(`g`);
    gsap.set(this.paths, {fill: this.props.color});
    gsap.set(this.g, {opacity: this.props.alpha?this.props.alpha:0.1});
    this.startAnimations();
  }

  componentDidUpdate(prevProps, prevState, snapshot){
      if(prevState.animation != this.state.animation){
        if(this.state.animation){
          this.startAnimations()
        }else{
          this.stopAnimations()
        }
      }
  }

  startAnimations(){
    gsap.killTweensOf(this.paths);
    for (let j = 0; j < this.paths.length; j++){
      if(j<3){
        gsap.to(this.paths[j], {duration: 0, scaleX: 1, transformOrigin:"50% 50%", delay: 0, ease: "none" });
        gsap.to(this.paths[j], {duration: 1, scaleX: 0.5, ease: "none", delay: 0.25*j, repeat: -1, yoyo: true });
      }else{
        gsap.to(this.paths[j], {duration: 0, scale: 1, transformOrigin:"50% 50%", delay: 0, ease: "none" });
        gsap.to(this.paths[j], {duration: 1, scale: 1.5, ease: "power1.out", delay: 0.25*j, repeat: -1, yoyo: true });
      }

    }
  }
  stopAnimations(){
    gsap.killTweensOf(this.paths);
    gsap.set(this.paths, {scale: 1, scaleX: 1, scaleY: 1});
  }

  handleWaypointEnter(props){
    this.setState({animation: true});
  }
  handleWaypointLeave(props){
    this.setState({animation: false});
  }

  render() {
    const dtid = this.props.dtid;

    return (
      <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
      >
        <div className={`esgp-dotlines`} ref={this.myRef} data-id={dtid} {...html(require(`!raw-loader!../../assets/dotlines.svg`))} />
      </Waypoint>
    );
  }
}
