import 'bootstrap/dist/css/bootstrap.css';
import './index.css';

import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app';
import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';








const lang = ``;
const dataURL = `data/data`+ lang +`.json`;
const ES6Promise = require("es6-promise");
ES6Promise.polyfill();
const axios = require('axios');

// function ready() {
// 	const root = document.getElementById('esgp');
// 	root.setAttribute('data-ua', navigator.userAgent);
// 	axios.get(dataURL)
// 	.then(response => {
// 		ReactDOM.render(<App data={response.data} />, root);
// 	})
// 	.catch(e => {
// 		console.log("error: ", e);
// 	})
// }


import data from '../static/data/data.json';
function ready() {
	const root = document.getElementById('esgp');
	root.setAttribute('data-ua', navigator.userAgent);
	ReactDOM.render(<App data={data} />, root);
	window.onload = function() {};
}


document.addEventListener("DOMContentLoaded", ready);